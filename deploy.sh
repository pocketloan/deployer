#!/bin/bash
kubectl apply -f k8s
kubectl set image deployments/activity-deployment activity=pocketloan/activity:latest
kubectl set image deployments/api-deployment api=pocketloan/api:latest
kubectl set image deployments/app-deployment app=pocketloan/app:latest
kubectl set image deployments/graphql-deployment graphql=pocketloan/graphql:latest
kubectl set image deployments/notification-deployment notify=pocketloan/notification:latest
kubectl set image deployments/transactions-deployment transactions=pocketloan/transactions:latest